package main

import (
	"fmt"

	"github.com/pmezard/go-difflib/difflib"
)

func main() {
	diff := difflib.UnifiedDiff{
		A:        difflib.SplitLines("foo\nbar\n"),
		B:        difflib.SplitLines("foo\nbaz\n"),
		FromFile: "Original",
		ToFile:   "Current",
		Context:  3,
	}
	text, _ := difflib.GetUnifiedDiffString(diff)
	fmt.Printf(text)
}
