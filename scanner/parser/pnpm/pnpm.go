package pnpm

import (
	"io"

	"gopkg.in/yaml.v2"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/scanner/parser"
)

// Parse scans a pnpm lock file and returns a list of packages
func Parse(r io.Reader, opts parser.Options) ([]parser.Package, []parser.Dependency, error) {
	lockfile := Lockfile{Options: opts}
	err := yaml.NewDecoder(r).Decode(&lockfile)
	if err != nil {
		return nil, nil, err
	}
	pkgs, err := lockfile.Parse()
	return pkgs, nil, err
}

func init() {
	parser.Register("pnpm", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypeNpm,
		Filenames:   []string{"pnpm-lock.yaml"},
	})
}
