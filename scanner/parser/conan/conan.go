package conan

import (
	"encoding/json"
	"errors"
	"io"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/scanner/parser"
)

var errParsingDependency = errors.New("Unable to parse dependency")

// Document is a Conan lock file
type Document struct {
	Version   string    `json:"version"`
	GraphLock GraphLock `json:"graph_lock"`
}

// GraphLock is the top level container for dependencies
type GraphLock struct {
	Nodes map[string]DependencyInfo `json:"nodes"`
}

// DependencyInfo describes a Conan dependency
type DependencyInfo struct {
	// PackageAndVersion is formatted as <packagename>/<version>@<user>/<channel>
	// For example: poco/1.9.4, openssl/1.1.1g or jerryscript/2.2.0@user/channel
	Ref string `json:"ref"`

	// List of dependencies referred by their ids in GraphLock
	Requires []string `json:"requires"`

	// List of build dependencies, available only in meta Node
	BuildRequires []string `json:"build_requires"`
}

const supportedFileFormatVersion = "0.4"

// Parse scans a Conan lock file and returns a list of packages
func Parse(r io.Reader, opts parser.Options) ([]parser.Package, []parser.Dependency, error) {
	document := Document{}
	err := json.NewDecoder(r).Decode(&document)
	if err != nil {
		return nil, nil, err
	}
	if document.Version != supportedFileFormatVersion {
		return nil, nil, parser.ErrWrongFileFormatVersion
	}

	pkgs := []parser.Package{}
	deps := []parser.Dependency{}
	packageMap := make(map[string]parser.Package, len(document.GraphLock.Nodes))
	requiresMap := map[string][]string{}
	runtimeDeps := map[string]bool{}

	buildDeps := map[string]bool{}
	directDeps := map[string]bool{}

	// find all direct deps, they are either runtime or build dependencies
	for _, ID := range document.GraphLock.Nodes["0"].Requires {
		directDeps[ID] = true
		runtimeDeps[ID] = true
		buildDeps[ID] = false
	}

	// the first node always contains a build_requires array which lists the development dependencies
	for _, ID := range document.GraphLock.Nodes["0"].BuildRequires {
		buildDeps[ID] = true
		// add build deps as direct as well
		directDeps[ID] = true
	}

	for i, d := range document.GraphLock.Nodes {
		name, version, err := d.packageAndVersion()
		if err != nil {
			continue
		}

		requiresMap[i] = d.Requires

		// add dependency as a build dependency
		// if no other runtime dependencies depend on it
		if buildDeps[i] {
			for _, ID := range d.Requires {
				if runtimeDeps[ID] == false {
					buildDeps[ID] = true
				}
			}
		}

		pkg := parser.Package{Name: name, Version: version}
		packageMap[i] = pkg

		pkgs = append(pkgs, pkg)
	}

	for i, reqs := range requiresMap {
		dependent := packageMap[i]
		dependentDev := buildDeps[i]
		if directDeps[i] {
			deps = append(deps, parser.Dependency{
				To:           &dependent,
				VersionRange: dependent.Version,
				// buildDeps contains requirements that are only needed when you need to
				// build a package from sources, such as dev tools, compilers, build systems,
				// code analyzers, testing libraries, etc.
				// See https://docs.conan.io/en/1.43/devtools/build_requires.html for more details.
				Dev: dependentDev,
			})
		}

		for _, depID := range reqs {
			dependency := packageMap[depID]
			deps = append(deps, parser.Dependency{
				From:         &dependent,
				To:           &dependency,
				VersionRange: dependency.Version,
				Dev:          dependentDev,
			})
		}
	}

	return pkgs, deps, nil
}

func (d DependencyInfo) packageAndVersion() (string, string, error) {
	packageAndUserChannel := strings.Split(d.Ref, "@")

	packageAndVersion := strings.Split(packageAndUserChannel[0], "/")

	if len(packageAndVersion) != 2 {
		return "", "", errParsingDependency
	}

	return packageAndVersion[0], packageAndVersion[1], nil
}

func init() {
	parser.Register("conan", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypeConan,
		Filenames:   []string{"conan.lock"},
	})
}
