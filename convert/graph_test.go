package convert

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/scanner/parser"
)

func TestGraph(t *testing.T) {
	pkgs := []parser.Package{
		{Name: "a", Version: "1"},     // 1
		{Name: "b", Version: "1.1"},   // 2
		{Name: "c", Version: "1.2"},   // 3
		{Name: "d", Version: "1.2.1"}, // 4
		{Name: "e", Version: "2"},     // 5
		{Name: "e", Version: "2"},     // 6
		{Name: "f", Version: "2.1"},   // 7
	}

	deps := []parser.Dependency{
		{
			To: &parser.Package{Name: "a", Version: "1"}, // 1
		},
		{
			From: &parser.Package{Name: "a", Version: "1"},   // 1
			To:   &parser.Package{Name: "b", Version: "1.1"}, // 2
		},
		{
			From: &parser.Package{Name: "a", Version: "1"},   // 1
			To:   &parser.Package{Name: "c", Version: "1.2"}, // 3
		},
		{
			From: &parser.Package{Name: "c", Version: "1.2"},   // 3
			To:   &parser.Package{Name: "d", Version: "1.2.1"}, // 4
		},
		{
			To: &parser.Package{Name: "e", Version: "2"}, // 5 or 6
		},
		{
			From: &parser.Package{Name: "e", Version: "2"},   // 5 or 6
			To:   &parser.Package{Name: "f", Version: "2.1"}, // 7
		},
		{
			From: &parser.Package{Name: "f", Version: "2.1"},   // 7
			To:   &parser.Package{Name: "d", Version: "1.2.1"}, // 4
		},
		{
			// self-edge
			From: &parser.Package{Name: "f", Version: "2.1"}, // 7
			To:   &parser.Package{Name: "f", Version: "2.1"}, // 7
		},
	}

	index := NewIndex(pkgs)
	graph := NewGraph(pkgs, deps, index)

	tcs := []struct {
		name  string
		pkg   parser.Package
		paths [][]Node // possible paths
	}{
		{
			"direct",
			parser.Package{Name: "a", Version: "1"},
			[][]Node{{}},
		},
		{
			"direct 2",
			parser.Package{Name: "e", Version: "2"},
			[][]Node{{}},
		},
		{
			"2nd level",
			parser.Package{Name: "b", Version: "1.1"},
			[][]Node{
				{
					{1, &parser.Package{Name: "a", Version: "1"}},
				},
			},
		},
		{
			"3rd level",
			parser.Package{Name: "d", Version: "1.2.1"},
			[][]Node{
				{
					{1, &parser.Package{Name: "a", Version: "1"}},
					{3, &parser.Package{Name: "c", Version: "1.2"}},
				},
				{
					{5, &parser.Package{Name: "e", Version: "2"}},
					{7, &parser.Package{Name: "f", Version: "2.1"}},
				},
				{
					{6, &parser.Package{Name: "e", Version: "2"}},
					{7, &parser.Package{Name: "f", Version: "2.1"}},
				},
			},
		},
	}

	t.Run("PathTo", func(t *testing.T) {
		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				got := graph.PathTo(tc.pkg)
				require.Contains(t, tc.paths, got)
			})
		}
	})
}
