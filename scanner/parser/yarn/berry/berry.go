package berry

import (
	"fmt"
	"io"
	"regexp"
	"strings"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/scanner/parser"
)

const (
	yarnV2LockFileVersion = 4
	yarnV3LockFileVersion = 6
)

var regexSpec = regexp.MustCompile(`"?(@?[^@]+)@([^"]*)"?`)

// Metadata section of the berry lock file
type Metadata struct {
	Version int `yaml:"version"`
}

// Spec contains the name and the version requirement
// for a definition
type Spec struct {
	Name        string `yaml:"-"`
	Requirement string `yaml:"-"`
}

func newSpec(rawSpec string) (*Spec, error) {
	specMatch := regexSpec.FindStringSubmatch(rawSpec)
	if specMatch == nil || len(specMatch) < 2 {
		return nil, fmt.Errorf("invalid spec: %s", rawSpec)
	}

	return &Spec{
		Name:        specMatch[1],
		Requirement: specMatch[2],
	}, nil
}

func (s Spec) isWorkspace() bool {
	return strings.HasPrefix(s.Requirement, "workspace:")
}

func (s Spec) isPatch() bool {
	return strings.HasPrefix(s.Requirement, "patch:")
}

// Definition contains all fields of a package defintion
// found in a berry lock file. For now we are only interested in
// the version
type Definition struct {
	Version string `yaml:"version"`
}

// Definitions contains packages definitions
type Definitions map[string]*Definition

// Lockfile represents a berry Yarn lock file
type Lockfile struct {
	Metadata    Metadata    `yaml:"__metadata"`
	Definitions Definitions `yaml:",inline"`
}

// Parse parses a berry yarn lock file and returns a list of packages
func Parse(r io.Reader) ([]parser.Package, []parser.Dependency, error) {
	var doc Lockfile
	if err := yaml.NewDecoder(r).Decode(&doc); err != nil {
		return nil, nil, fmt.Errorf("failed to parse yarn.lock as a yaml file: %w", err)
	}

	// parse metadata
	err := doc.Metadata.Validate()
	if err != nil {
		return nil, nil, err
	}

	pkgs := []parser.Package{}
	for key, def := range doc.Definitions {

		// A key can be in the form of comma separated <package_name>@<version_requirement> like:
		// "@babel/code-frame@npm:^7.0.0, @babel/code-frame@npm:^7.10.4":
		// We only need to keep the first package_name and version_requirement
		specList := strings.Split(key, ", ")
		firstSpec, err := newSpec(specList[0])
		if err != nil {
			return nil, nil, err
		}

		switch {
		case firstSpec.isWorkspace():
			log.Warnf("Yarn workspace are not supported, skipping '%s%s'", firstSpec.Name, firstSpec.Requirement)
		case firstSpec.isPatch():
			log.Debugf("Yarn patch is skipped '%s%s'", firstSpec.Name, firstSpec.Requirement)
		default:
			pkgs = append(pkgs, parser.Package{Name: firstSpec.Name, Version: def.Version})
		}

	}

	return pkgs, nil, nil
}

// Validate checks if the berry lock file version is supported
func (m Metadata) Validate() error {
	// Check for unsupported yarn.lock file versions
	if m.Version < yarnV2LockFileVersion || m.Version > yarnV3LockFileVersion {
		return fmt.Errorf("unsupported yarn.lock file version %d", m.Version)
	}

	log.Debugf("Yarn berry, lock file version = %v", m.Version)
	return nil
}
