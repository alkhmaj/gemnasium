package manifest

import (
	"encoding/json"
	"os"
	"path"
	"path/filepath"
	"sort"
	"time"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/cyclonedx"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/finder"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

const (
	manifestFileName = "sbom-manifest.json"
	manifestVersion  = "0.0.1"
)

// Create generates a manifest file and outputs it to the artifactDir
func Create(artifactDir string, sboms []cyclonedx.SBOM, reportScanner report.ScannerDetails) error {
	manifest := Manifest{
		Version:   manifestVersion,
		Timestamp: Time{time.Now()},
		Analyzer: Analyzer{
			ID:      reportScanner.ID,
			Version: reportScanner.Version,
		},
	}

	for _, sbom := range sboms {
		manifest.addComponent(sbom)
	}

	manifest.sortComponents()

	return manifest.write(artifactDir)
}

func (manifest *Manifest) write(artifactDir string) error {
	absoluteManifestPath := filepath.Join(artifactDir, manifestFileName)
	manifestFile, err := os.Create(absoluteManifestPath)
	if err != nil {
		return err
	}
	defer manifestFile.Close()

	encoder := json.NewEncoder(manifestFile)
	encoder.SetIndent("", "  ")

	return encoder.Encode(manifest)
}

func (manifest *Manifest) addComponent(sbom cyclonedx.SBOM) {
	component := Component{
		Project: Project{
			Path: path.Dir(sbom.InputFilePath),
		},
		Language:       finder.LanguageForPackageManager(sbom.PackageManager),
		PackageManager: sbom.PackageManager,
		PackageType:    sbom.PackageType,
		Files: []File{
			{
				Type: "sbom",
				Path: sbom.OutputFilePath,
			},
			{
				Type: "input",
				Path: sbom.InputFilePath,
			},
		},
	}

	manifest.Components = append(manifest.Components, component)
}

// sortComponents sorts components by project.path, then by package_type
func (manifest *Manifest) sortComponents() {
	sort.Slice(manifest.Components, func(i, j int) bool {
		ni, nj := manifest.Components[i].Project.Path, manifest.Components[j].Project.Path
		if ni == nj {
			return manifest.Components[i].PackageType < manifest.Components[j].PackageType
		}
		return ni < nj
	})
}
