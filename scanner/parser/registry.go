package parser

import (
	"sort"
	"sync"
)

var parsersMu sync.Mutex
var parsers = make(map[string]Parser)

// Register registers a parser with a name.
func Register(name string, parser Parser) {
	parsersMu.Lock()
	defer parsersMu.Unlock()
	if _, dup := parsers[name]; dup {
		panic("Register called twice for name " + name)
	}
	parsers[name] = parser
}

// Lookup looks for a parser compatible with the given filename.
func Lookup(filename string) *Parser {
	parsersMu.Lock()
	defer parsersMu.Unlock()
	for _, p := range parsers {
		for _, f := range p.Filenames {
			if f == filename {
				return &p
			}
		}
	}
	return nil
}

// Parsers returns a sorted list of the names of the registered parsers.
func Parsers() []string {
	parsersMu.Lock()
	defer parsersMu.Unlock()
	list := make([]string, 0, len(parsers))
	for name := range parsers {
		list = append(list, name)
	}
	sort.Strings(list)
	return list
}

// PackageTypes returns all the registered package types, without duplicates.
func PackageTypes() []string {
	parsersMu.Lock()
	defer parsersMu.Unlock()

	// collect package types in a map to remove duplicates
	pkgTypeMap := map[string]bool{}
	for _, parser := range parsers {
		pkgTypeMap[string(parser.PackageType)] = true
	}

	// turn map keys into a slice of strings
	pkgTypes := []string{}
	for pkgType := range pkgTypeMap {
		pkgTypes = append(pkgTypes, pkgType)
	}

	sort.Strings(pkgTypes)
	return pkgTypes
}
