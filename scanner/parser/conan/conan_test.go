package conan

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/scanner/parser/testutil"
)

func TestConan(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		t.Run("wrong version", func(t *testing.T) {
			fixture := testutil.Fixture(t, "wrong_version", "conan.lock")
			_, _, err := Parse(fixture, parser.Options{})
			require.EqualError(t, err, parser.ErrWrongFileFormatVersion.Error())
		})

		for _, tc := range []string{"simple", "big", "deep"} {
			t.Run(tc, func(t *testing.T) {
				fixture := testutil.Fixture(t, tc, "conan.lock")
				pkgs, deps, err := Parse(fixture, parser.Options{})
				require.NoError(t, err)
				testutil.RequireExpectedPackages(t, tc, pkgs)
				testutil.RequireExpectedDependencies(t, tc, deps)
			})
		}
	})
}
